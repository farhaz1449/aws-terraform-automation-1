resource "kubernetes_service" "frontend_svc" {
  metadata {
    name = "frontend"
  }
  spec {
    selector = {
      test = kubernetes_deployment.frontend.metadata.0.labels.test
    }
    port {
      port        = 3000
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_service" "backend_svc" {
  metadata {
    name = "backend"
  }
  spec {
    selector = {
      test = kubernetes_deployment.backend.metadata.0.labels.test
    }
    port {
      port        = 5000
    }

    type = "NodePort"
  }
}

