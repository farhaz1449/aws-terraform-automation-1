terraform {
  backend "s3" {
    bucket = "ts4u-test-bucket-1"                       # Bucket name from S3
    key    = "terraform.tfstate"       # This is the path or directory terraform will create in S3 bucket
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.39.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.15.0"
    }

    helm = {
      source = "hashicorp/helm"
      version = "2.7.1"
    }
  }
}

