resource "kubernetes_deployment" "backend" {
  metadata {
    name = "backend"
    labels = {
      test = "backend"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        test = "backend"
      }
    }

    template {
      metadata {
        labels = {
          test = "backend"
        }
      }

      spec {
        container {
          image = "farhaz1449/backend-opt"
          name  = "backend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      test = "frontend"
    }
  }

  spec {
    replicas = 5

    selector {
      match_labels = {
        test = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          test = "frontend"
        }
      }

      spec {
        container {
          image = "farhaz1449/frontend-opt"
          name  = "frontend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}


